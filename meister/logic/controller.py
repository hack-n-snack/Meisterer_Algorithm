from matplotlib import pyplot as plt

import numpy as np

from meister.config import Config
from meister.logic import in_out, timedomain_fx, specdomain_fx, multibandcomp_fx


def do_mastering(path_in, path_ref, out_path=None):
    x_in_stereo, x_ref_stereo = in_out.read_in_audio(path_in), in_out.read_in_audio(
        path_ref
    )

    x_in_stereo = timedomain_fx.limit_and_normalize(x_in_stereo)
    x_ref_stereo = timedomain_fx.limit_and_normalize(x_ref_stereo)

    x_in_stereo = specdomain_fx.align_spectrum(x_in_stereo, x_ref_stereo)
    x_in_stereo = timedomain_fx.limit_and_normalize(x_in_stereo)

    x_in_stereo = timedomain_fx.do_histogram_matching(x_in_stereo, x_ref_stereo)

    in_out.write_out_audio(x_in_stereo, out_path)

    return x_in_stereo
