import soundfile as sf
import librosa
import numpy as np
import os

from meister.config import Config


def read_in_audio(path_to_audio):
    x_in, sr = librosa.load(path_to_audio, sr=Config.sr, mono=False)

    if x_in.ndim < 2:
        x_in = np.tile(x_in, (2, 1))

    return x_in


def write_out_audio(x, filepath_out=None):
    if not filepath_out:
        filepath_out = os.path.join(Config.out_path, "mastered.wav")
    sf.write(filepath_out, np.transpose(x), 44100, "PCM_16")
