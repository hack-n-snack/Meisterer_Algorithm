import librosa
import numpy as np
from matplotlib import pyplot as plt

from meister.config import Config
from meister.logic import timedomain_fx


def align_spectrum(x_in_stereo, x_ref_stereo):
    x_in_mono_exc = timedomain_fx.get_loudest_section_in_mono(x_in_stereo)
    x_ref_mono_exc = timedomain_fx.get_loudest_section_in_mono(x_ref_stereo)

    X_in_avg_exc = get_average_spectrum(x_in_mono_exc)
    X_ref_avg_exc = get_average_spectrum(x_ref_mono_exc)

    X_filter_raw = cal_spectrum_of_filter_kernel(X_ref_avg_exc, X_in_avg_exc)
    x_filter, X_filter_adapted = spectralImpulseToFilterKernel(X_filter_raw)

    x_in_stereo_filtered = timedomain_fx.convolve(x_in_stereo, kernel=x_filter)

    return x_in_stereo_filtered


def plot_spectra(X_in_avg_exc, X_ref_avg_exc, X_filter_adapted):

    support = np.linspace(0, int(Config.sr / 2), X_in_avg_exc.size)

    plt.plot(support, X_in_avg_exc / np.max(X_in_avg_exc))
    plt.plot(support, X_ref_avg_exc / np.max(X_ref_avg_exc))
    plt.plot(support, X_filter_adapted / np.max(X_filter_adapted))

    plt.plot()
    plt.xscale("log")

    return 0


def get_average_spectrum(x_mono):

    X_comp = librosa.stft(
        x_mono,
        n_fft=Config.fft_n_samples,
        win_length=Config.fft_winglength_samples,
        hop_length=Config.fft_hoplength_samples,
    )
    X_average = np.mean(np.abs(X_comp), axis=1)
    return X_average


def spectralImpulseToFilterKernel(spec_impulse_mag):
    spec_impulse_cartesian = pol2cart(spec_impulse_mag)
    spec_impulse_cartesian = np.concatenate(
        (spec_impulse_cartesian, np.flip(spec_impulse_cartesian)[1:-1])
    )
    kernel = np.fft.ifft(spec_impulse_cartesian).real
    kernel_shifted = np.roll(kernel, int(Config.filter_kernel_length / 2))
    kernel_truncated = kernel_shifted[: Config.filter_kernel_length + 1]
    kernel_windowed = kernel_truncated * np.hamming(Config.filter_kernel_length + 1)

    return (kernel_windowed, manual_fft(kernel_windowed))


def cart2pol(real, im):
    rho = np.sqrt(real ** 2 + im ** 2)
    phi = np.arctan2(real, real)
    return (rho, phi)


def pol2cart(mag, phi=None):
    phi = np.zeros(mag.size) if phi is None else phi

    real = mag * np.cos(phi)
    im = mag * np.sin(phi)
    cartesian = real + 1j * im

    return cartesian


def manual_fft(x):

    x = np.concatenate((x, np.zeros(Config.fft_n_samples - x.size)))
    X = np.fft.fft(x)
    X = np.abs(X)[: int(Config.fft_n_samples / 2 + 1)]

    return X


def cal_spectrum_of_filter_kernel(X_goal, X_in):
    return X_goal / X_in
