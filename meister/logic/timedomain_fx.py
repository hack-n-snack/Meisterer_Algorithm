import librosa
import numpy as np
from tqdm import tqdm
from matplotlib import pyplot as plt

from meister.config import Config


def stereo_to_mono(x_stereo):

    return librosa.to_mono(x_stereo)


def limit_signal(x, quantile=None):
    quantile = Config.limiting_cutoff_quantile if quantile is None else quantile

    cutoff = np.quantile(np.abs(x), quantile)
    x[x > cutoff] = cutoff
    x[x < -cutoff] = -cutoff
    return x


def normalize(x):
    return x / np.max(np.abs(x))


def limit_and_normalize(x):
    return normalize(limit_signal(x))


def get_loudest_section_in_mono(x_stereo):
    return get_loudest_section(stereo_to_mono(x_stereo))


def get_loudest_section(x_mono):

    if Config.section_duration_for_comparison_in_s == -1:
        return x_mono

    frame_length_in_samples = int(Config.section_window_length_in_s * Config.sr)
    section_length_in_samples = int(
        Config.section_duration_for_comparison_in_s * Config.sr
    )
    section_length_in_frames = int(
        Config.section_duration_for_comparison_in_s / Config.section_window_length_in_s
    )

    x_mono = x_mono[0 : (x_mono.size - (x_mono.size % frame_length_in_samples))]

    start_samples_of_frames = np.arange(0, x_mono.shape[0], frame_length_in_samples)

    x_mono_framed = np.reshape(x_mono, (-1, frame_length_in_samples))
    loudness_of_frames = np.sqrt(np.mean(x_mono_framed ** 2, axis=1))
    loudness_of_sections = np.asarray(
        [
            np.sum(loudness_of_frames[i : i + section_length_in_frames])
            for i in range(loudness_of_frames.size)
        ]
    )
    start_sample_of_loudest_section = start_samples_of_frames[
        np.argmax(loudness_of_sections)
    ]

    x_loudest_section = x_mono[
        start_sample_of_loudest_section : start_sample_of_loudest_section
        + section_length_in_samples
    ]

    return x_loudest_section


def convolve(x, kernel):
    if x.ndim == 1:
        return np.convolve(x, kernel)

    if x.ndim == 2:
        left_convolved = np.convolve(x[0], kernel)
        right_convolved = np.convolve(x[1], kernel)
        return np.stack([left_convolved, right_convolved])


def quantize(x):
    x_quantized = (x * 2 ** (Config.bit_depth - 1)).astype(int)

    # bins = get_quantization_support(range="quantization")
    # bin_mapping = np.digitize(x, bins)
    # bin_mapping = bin_mapping - 1
    # bin_mapping[bin_mapping == bins.size] = bins.size - 1
    # x_quantized = bins[bin_mapping]
    # return x_quantized

    return x_quantized


def dequantize(x):
    return normalize(x.astype(float))


def do_histogram_matching(x_in_stereo, x_ref_stereo):
    x_in_mono_excerpt = get_loudest_section_in_mono(x_in_stereo)
    x_ref_mono_exerpt = get_loudest_section_in_mono(x_ref_stereo)

    x_in_stereo_quantized = quantize(x_in_stereo)
    x_in_mono_excerpt_quantized = quantize(x_in_mono_excerpt)
    x_ref_mono_excerpt_quantized = quantize(x_ref_mono_exerpt)

    transfer_function = get_transfer_function(
        x_in_mono_excerpt_quantized, x_ref_mono_excerpt_quantized
    )

    x_in_stereo_compressed = apply_transfer_function(
        x_in_stereo_quantized, transfer_function
    )

    x_in_stereo_compressed = dequantize(x_in_stereo_compressed)

    return x_in_stereo_compressed


def get_transfer_function(x_in_mono_excerpt_quantized, x_ref_mono_excerpt_quantized):
    vals, x_in_hist = cal_histogram(x_in_mono_excerpt_quantized)
    vals, x_ref_hist = cal_histogram(x_ref_mono_excerpt_quantized)
    transfer_function = cal_transfer_function(vals, x_in_hist, x_ref_hist)
    return transfer_function


def apply_transfer_function(x, vals_out):
    vals_out_posandneg = np.concatenate([vals_out, np.flip(-vals_out)[:-1]])
    x_compressed = vals_out_posandneg[x]

    return x_compressed


def cal_histogram(x):

    # bins = get_quantization_support(range="histogram_bining")

    bins_alt = np.arange(0, 2 ** (Config.bit_depth - 1) + 2)
    counts, vals = np.histogram(np.abs(x), bins_alt)

    counts_cum = np.cumsum(counts)
    counts_cum = counts_cum / np.max(counts_cum)
    return vals[:-1], counts_cum


def cal_transfer_function(vals, hist_in, hist_ref):
    transfer_function = np.zeros(vals.shape)
    for i in range(vals.size):
        corresponding_arg = np.argmin(np.abs(hist_ref - hist_in[i]))
        transfer_function[i] = vals[corresponding_arg]

    transfer_function = zero_connect_transfer_function(transfer_function)
    transfer_function = mitigate_transfer_function(transfer_function)
    # transfer_function = limit_slope_of_transfer_function(transfer_function)

    return transfer_function


def limit_slope_of_transfer_function(transf_in):

    transf_out = np.diff(transf_in, prepend=0)
    transf_out = np.clip(transf_out, 0, Config.transfer_function_maxslope)
    transf_out = np.cumsum(transf_out)
    transf_out = transf_out + np.max(transf_in) - np.max(transf_out)
    return transf_out.astype(int)


def mitigate_transfer_function(transf_in):
    straight_line = np.arange(0, 2 ** (Config.bit_depth - 1) + 1)
    over_or_under = np.sign(transf_in - straight_line)
    transf_out = (
        over_or_under
        * np.abs(transf_in - straight_line) ** Config.transfer_mitigation_exponent
        + straight_line
    )
    return transf_out.astype(int)


def zero_connect_transfer_function(transf_in):

    support = np.arange(0, 2 ** (Config.bit_depth - 1) + 1)
    transfer_alt = (
        np.clip(
            support * Config.transfer_zero_connection_slope, 0, a_max=np.max(support)
        )
    ).astype(int)

    transf_out = np.min(np.stack([transf_in, transfer_alt]), axis=0)
    return transf_out.astype(int)
