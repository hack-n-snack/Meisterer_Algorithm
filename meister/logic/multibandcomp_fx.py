from scipy import signal
import numpy as np
import librosa
from matplotlib import pyplot as plt

from meister.logic import timedomain_fx
from meister.config import Config


def do_multiband_compressing(x_in_stereo, x_ref_stereo):
    x_in_mono_excerpt = timedomain_fx.get_loudest_section_in_mono(x_in_stereo)
    x_ref_mono_exerpt = timedomain_fx.get_loudest_section_in_mono(x_ref_stereo)

    x_in_stereo_quantized = timedomain_fx.quantize(x_in_stereo)
    x_in_mono_excerpt_quantized = timedomain_fx.quantize(x_in_mono_excerpt)
    x_ref_mono_excerpt_quantized = timedomain_fx.quantize(x_ref_mono_exerpt)

    result = [
        adapt_freqband(
            cutoffs,
            x_in_mono_excerpt_quantized,
            x_ref_mono_excerpt_quantized,
            x_in_stereo_quantized,
        )
        for cutoffs in Config.bands_hz
    ]
    result = np.sum(np.stack(result), axis=0)

    result = timedomain_fx.dequantize(result)

    return result


def adapt_freqband(cutoffs, x_in_mono_excerpt, x_ref_mono_exerpt, x_in_stereo):

    if cutoffs[0] == "lp":
        sos = signal.butter(
            Config.order, cutoffs[1], "lowpass", fs=Config.sr, output="sos"
        )
    if cutoffs[0] == "hp":
        sos = signal.butter(
            Config.order, cutoffs[1], "highpass", fs=Config.sr, output="sos"
        )
    if cutoffs[0] == "bp":
        sos = signal.butter(
            int(Config.order / 2),
            (cutoffs[1], cutoffs[2]),
            "bandpass",
            fs=Config.sr,
            output="sos",
        )

    x_in_excerpt_filtered = signal.sosfilt(sos, x_in_mono_excerpt).astype(int)
    x_ref_excerpt_filtered = signal.sosfilt(sos, x_ref_mono_exerpt).astype(int)
    x_in_stereo_filtered = signal.sosfilt(sos, x_in_stereo, axis=-1).astype(int)

    transfer_function = timedomain_fx.get_transfer_function(
        x_in_excerpt_filtered, x_ref_excerpt_filtered
    )

    x_in_stereo_compressed = timedomain_fx.apply_transfer_function(
        x_in_stereo_filtered, transfer_function
    )

    return x_in_stereo_compressed


def view_average_spectrum(x):
    x = timedomain_fx.normalize(x)

    if x.ndim == 2:
        x = timedomain_fx.stereo_to_mono(x)

    support = np.linspace(0, Config.sr / 2, int((Config.fft_n_samples) / 2 + 1))

    s = np.abs(librosa.stft(x, n_fft=Config.fft_n_samples))
    s_avg = np.mean(s, axis=1)
    plt.plot(support, s_avg)


def extract_freqband():
    n_fft = 1024
    order = 12

    x_mono = np.zeros((n_fft,))
    x_mono[0] = 1

    support = np.linspace(0, Config.sr / 2, int((n_fft) / 2 + 1))
    spec_in = np.abs(np.fft.fft(x_mono, n_fft))[0 : int((n_fft) / 2 + 1)]

    sos = signal.butter(order, 10000, "hp", fs=Config.sr, output="sos")
    lp_filtered = signal.sosfilt(sos, x_mono)
    spec_lp = np.abs(np.fft.fft(lp_filtered, n_fft))[0 : int((n_fft) / 2 + 1)]

    sos = signal.butter(
        int(order / 2), (5000, 10000), "bandpass", fs=Config.sr, output="sos"
    )
    hp_filtered = signal.sosfilt(sos, x_mono)

    spec_hp = np.abs(np.fft.fft(hp_filtered, n_fft))[0 : int((n_fft) / 2 + 1)]

    reconstructed = lp_filtered + hp_filtered

    spec_rec = np.abs(np.fft.fft(reconstructed, n_fft))[0 : int((n_fft) / 2 + 1)]
    pass
