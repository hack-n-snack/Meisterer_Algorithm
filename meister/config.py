class Config:

    sr = 44100

    fft_n_samples = 2048
    fft_winglength_samples = fft_n_samples
    fft_hoplength_samples = int(fft_n_samples * 0.5)

    limiting_cutoff_quantile = 0.9999

    filter_kernel_length = 80

    bit_depth = 16
    transfer_function_maxslope = 10000
    transfer_zero_connection_slope = 3
    transfer_mitigation_exponent = 0.98

    section_duration_for_comparison_in_s = 60
    section_window_length_in_s = 0.1

    bands_hz = [("lp", 500), ("bp", 500, 5000), ("hp", 5000)]
    order = 12

    out_path = "./"
